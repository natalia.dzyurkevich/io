;
; make everything about dust: n, crosssection, mass... with or without ice
;
pro io_do_fdg, ppp,x1,x2, snow, ngas, $
               f_dg_t,ndust,mud1,ava1,sigma1, fidu

; parameters: common for all cases:
         ava_0= 1.d-5
         ava = 20.d-5 ;! in cm
         ava_si=ava_0
         Nnum=(ava/ava_0)^2
         sigma = !pi* ava^2 * 1.d0 ;! (see after Eq.51)
         Numm=(ava/ava_0)^2
         f_dg_si=   0.43
         f_dg_h2o=  0.94
        dust0= 1.4d0  ; in g cm^-3
        dust_si=3.3d0
        dust_h2o=0.92d0
        sigma0=!pi*ava_0^2
        mud = (4.*!pi/3.)*(ava_0)^3*dust0*Nnum ; (see after Eq.46) ; is changed for some cases
        mud_si=mud
        mum=2.3333 * 1.67262e-24 ; meanw * mp 

; restore grid size
szgas=size(ngas)
in=szgas(1) & jn=szgas(2) & kn=szgas(3)


; Create here a radial profile of dust-to-gas ratio
;
;  fidu=0 > make overall constant dust-to-gas ratio
;  fidu=1 > make simple transition from pure Si to ice-covered Si (mass increases beyond snow line)
;  fidu=2 > make fit to 'evolved' f_dg profile as in Til's 2010 paper
;  fidu=3 > add to 3rd case the ice mantles, also this is may be too rude..
;

; _t total dust-to-gas ratio is array to be created here.

f_dg_t=dblarr(in,jn) & f_dg_t(*,*)=0.e0

; create also ndust array and mud1 - mass of the dust, ava1 - mean effective cross-section

ndust=dblarr(in,jn,kn) & ndust(*,*,*)=0.e0
mud1=dblarr(in,jn) & mud1(*,*)=0.e0
ava1=dblarr(in,jn) & ava1(*,*)=0.e0
sigma1=dblarr(in,jn) & sigma1(*,*)=0.e0

ava1(*,*)=ava
sigma1(*,*)=sigma
mud1(*,*)=mud

; this is a 'common' dust depletion.
         f_dg=10.^(-ppp)

; consider various cases................
; (0)
if (fidu eq 0) then begin
     for j=0,jn-1 do begin
      for i=0,in-1 do begin
         f_dg_t(i,j)=f_dg
         ndust(i,j,*)=ngas(i,j,*)*mum*f_dg_t(i,j)/mud1(i,j)
      endfor
     endfor
endif ; case 0


;(1)
if (fidu eq 1) then begin

print, 'DO f_dg: '

         f_dg_si=  0.43
         f_dg_h2o= 0.94
         ava0_si=ava_0
         mud_si=4./3.*!pi*(ava0_si)^3* dust_si ; calcullate only monomers weight

 for k=0,kn-1 do begin
  for i=0,in-1 do begin
   for j=0,jn-1 do begin
    fdg1=f_dg_si*(-snow(i,j)+1.d0)+snow(i,j)*(f_dg_si+f_dg_h2o)
    f_dg_t(i,j)=f_dg*fdg1

      rgas=ngas(i,j,k)*mum
      ndust_si=rgas*f_dg_si*f_dg/mud_si ; number density if Si monomers
      
      Da0_tp3=( (rgas*f_dg_t(i,j))/(ndust_si*4./3.*!pi) + ava0_si^3*( -dust_si) ) / dust_h2o
      a0_tp3=ava0_si^3+Da0_tp3
      a0_t=(a0_tp3)^(1./3.)

    ava1(i,j)=a0_t*sqrt(Nnum) ; see Okuzumi 09 eq. 51 (for fractals with D=2)
    sigma1(i,j)=!pi* ava1(i,j)^2
    mud1(i,j) = 4./3.*!pi* (dust_si*ava0_si^3 + dust_h2o* (a0_tp3 - ava0_si^3 ) )*Nnum 
    ndust(i,j,k)= rgas * f_dg_t(i,j) / mud1(i,j)  ; number density of fluffy aggregates, with Si core and ice mantle 
   endfor
  endfor
 endfor

; ..testing
;for i=0, in-1 do begin
;rgas=ngas(i,jn/2,kn/2)*mum
;ndust_si=rgas*f_dg_si*f_dg/mud_si
;
;print, i, snow(i,jn/2), '  ava1 r', ava1(i,jn/2) $
;, '  sigma r', sigma1(i,jn/2) $
;, '  a0_tp3 ', ( (rgas*f_dg_t(i,jn/2))/(ndust_si*4./3.*!pi) + ava0_si^3*(-dust_si) ) / dust_h2o $
;;, '  f_dg_t r', f_dg_t(i,jn/2) $
;, '  mud1 r', mud1(i,jn/2)
;endfor

endif ; case 1


;(2-3)
if (fidu eq 2 ) then begin

  ; parameters for the fit (best for f_dg=10^-4)..........
         he = alog10(4.)
         pointr=[0.2,0.5,1.,2.,3.,20.,200.]
         pointrl=alog10(pointr)
         pointf=[0.39,0.39,0.38,0.1,0.01, 2.d-3, 1.d-2]

         f_dg0=1.d-2       
         
  for i=0,in-1 do begin
   for j=0,jn-1 do begin
         x1_log=alog10(x1(i)*sin(x2(j)))
         fdg1=(  1.d0 - (f_dg0 - pointf(5))/f_dg0 *( exp(-(x1_log-pointrl(5))^2/0.25/he^2 )^0.15)   $
               + pointf(0)/f_dg0*10.^(-(x1_log-pointrl(0))^6) $
              )         
         f_dg_t(i,j)=f_dg*fdg1 
         mud=4./3.*!pi*(ava_0)^3*dust0*Nnum ; mass of flyffy aggregate, D=2, of mixed composition
         ndust(i,j,*)=ngas(i,j,*)*mum*f_dg_t(i,j)/mud
         ; ava, sigma are done above         
   endfor
  endfor

endif ; case 2

;(3)
if (fidu eq 3) then begin

         f_dg_si=  0.43
         f_dg_h2o= 0.94
         ava0_si=ava_0
         mud_si=4./3.*!pi*(ava0_si)^3 * dust_si; calcullate only monomers weight

          ; parameters for the fit (best for f_dg=10^-4)..........
         he = alog10(4.)
         pointr=[0.2,0.5,1.,2.,3.,20.,200.]
         pointrl=alog10(pointr)
         pointf=[0.39,0.39,0.38,0.1,0.01, 2.d-3, 1.d-2]

         f_dg0=1.d-2

 for k=0,kn-1 do begin
  for i=0,in-1 do begin
   for j=0,jn-1 do begin
     x1_log=alog10(x1(i)*sin(x2(j)))
     fdg1=(  1.d0 - (f_dg0 - pointf(5))/f_dg0 *( exp(-(x1_log-pointrl(5))^2/0.25/he^2 )^0.15)   $
               + pointf(0)/f_dg0*10.^(-(x1_log-pointrl(0))^6) $
              )
     f_dg_t(i,j)=f_dg* fdg1*( f_dg_si*(-snow(i,j)+1.d0)+snow(i,j)*(f_dg_si+f_dg_h2o))

     rgas=ngas(i,j,k)*mum
      ndust_si=rgas* f_dg * fdg1 * f_dg_si /mud_si ; number density if Si monomers

      a0_tp3=( (rgas*f_dg_t(i,j))/(ndust_si*4./3.*!pi) + ava0_si^3*( dust_h2o-dust_si) ) / dust_h2o
      a0_t=(a0_tp3)^(1./3.)

    ava1(i,j)=a0_t*sqrt(Nnum) ; see Okuzumi 09 eq. 51 (for fractals with D=2)
    sigma1(i,j)=!pi* ava1(i,j)^2
    mud1(i,j) = 4./3.*!pi * (dust_si*ava0_si^3 + dust_h2o* (a0_tp3 - ava0_si^3 ) )*Nnum
    ndust(i,j,k)= rgas * f_dg_t(i,j) / mud1(i,j)  ; number density of fluffy aggregates, with Si core and ice mantle    
 
   endfor
  endfor
 endfor

endif ; case 3

print, 'DUST finished, CASE=', fidu

end
