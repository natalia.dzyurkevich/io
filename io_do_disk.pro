pro io_do_disk,in,jn,kn,x1,x2,rin,temp0, Mdisk,pfak,rcut, $
               ngas,sound,verzh,omega1

;    do I need d and d0 output, or everywhere ngas is enough? ;

; make the hydro-static model of the gaseous disk
;
     
     G     =6.6726d-8
     unit_m=2.e+33  ; solar mass in g
     Msun=unit_m
     au    =1.496d+13 ; length unit, AU in cm
     mp=1.67262e-24
     meanw= 2.3333
     k_B = 1.3806d-16 ; ;! in cm^2 g s-2 K-1
     N_A = 6.0d+23
     ua=au
     mum=mp*meanw


d     =dblarr(in,jn,kn)
sound =dblarr(in,jn,kn)
verzh =dblarr(in,jn)
omega1=dblarr(in,jn) 
ngas  =dblarr(in,jn,kn)
snow  =dblarr(in,jn)

dsurf30=(2.-pfak)*Mdisk/ (2.*!pi*rcut^2*au^2)
dsurf00=dsurf30*(1./rcut)^(-pfak)*exp(-(1./rcut)^(2.-pfak))
;
print, 'check DO_DISK', Mdisk, dsurf30, dsurf00
sound0=sqrt(N_A*k_B/meanw *temp0/sqrt(rin )  )
omega0=sqrt( G*unit_m/rin/ua)/( rin*au )
ciso0= sound0 / (omega0*rin*au)
;
;units of the density
d0=dsurf00/(sqrt(2.*!pi)*ciso0*ua*rin )


Psat=0.d0
x1b=x1
x2b=x2

for i=0,in-1 do begin
for k=0,kn-1 do begin
for j=0,jn-1 do begin

temper= temp0/sqrt( x1b(i)*sin(x2b(j)))
sound(i,j,k)=sqrt(N_A*k_B/meanw *temp0/sqrt( x1b(i)*sin(x2b(j)) )  )
omega1(i,j)=sqrt( G*Msun/x1b(i)/ua/sin(x2b(j))  ) /( x1b(i)*sin(x2b(j))*ua )
ciso= sound(i,j,k) / (omega1(i,j)*x1b(i)*ua)
verzh(i,j)= sound(i,j,k) / (omega1(i,j)*x1b(i)*ua)
;;;;forneal2(i,j)=(x2b(j)-!pi/2.)/verzh(i,j)
;;;bbb(i,j)=(x2b(j)-!pi/2.)/verzh(i,j)
;

surf1= dsurf30 *(x1b(i)/rcut)^(-pfak)*exp(-(x1b(i)/rcut)^(2.-pfak))
;
d(i,j,k)=surf1/( sqrt(2.*!pi)*ciso*x1b(i)*ua*sin(x2b(j)) )/ d0 $
 * exp( - (cos(x2b(j))/( sqrt(2.)*ciso*sin(x2b(j)) ))^2  )
;
ngas(i,j,k)=d(i,j,k)*d0/mum
;
;snow and metal lines are calculated in the main ionizat-ch2.pro
;pressure=d(i,j,k)*d0*sound(i,j,k)^2* 1.2d-3
;

endfor
endfor
endfor

print, 'DISK finished'

end
