; guess initial Mg in gas phase
; see Dzyurkevich et al 2013ApJ...765..114D
; see Appendix B
;
;
pro io_Mg_init, metals, sound, ioe, ngas, ndust, sigma1, $
                betat1, mion1, nimg,nimg2,nh2

; recover grid size
  
ijkn=size(sound)
in=ijkn(1) & jn=ijkn(2) & kn=ijkn(3)

;parameters
; comment  nh2=ntot/(1.+2.*0.0851) rest is helium
;
;  ... eV is is cm^2 g /s^2
   mp=1.67262e-24
   me = 9.109d-28
   meanw= 2.3333
   mum=meanw*mp
   k_B = 1.3806d-16 ; ;! in cm^2 g s-2 K-1
   N_A = 6.0d+23

   kcalpm = 4.19/N_A*1.d+3*1.d+4
   eV=1.602e-19*1.d+3*1.d+4
   ED = 5300.;      *kcalpm
   DeltaE= 2.d-3*eV/k_B ;!!
   mion=24.3050*mp
   nsites=1.5d+15
;   metals= 6.78e-07 ;; 1.d-11 ; (Ilgner & Nelson '06)   6.78e-07 ;(in Dzyurkevich et al 2013?)
   mmet=24.3050*mp
   mhco=29.*mp   

betat1=dblarr(in,jn,kn) & mion1=dblarr(in,jn,kn)
nimg1=dblarr(in,jn,kn)  & ni_max=dblarr(in,jn,kn)
nimg=dblarr(in,jn,kn) & nimg2=dblarr(in,jn,kn) & nh2=dblarr(in,jn,kn)  


      for i=0,in-1 do begin
       for k=0,kn-1 do begin
        for j=0,jn-1 do begin
;
          temper=sound(i,j,k)^2*meanw/(N_A*k_B)

          ui=sqrt(8.*sound(i,j,k)^2*mum/!Pi/mion)
          ue=ui*sqrt(mion/me)   ;! in cm /s
  
          alpha=3.d-9
          beta11=3.d-11/sqrt(temper)
          beta22=3.d-6/sqrt(temper)


         ; see eqs. A1-A4
         smg=sqrt(ED*DeltaE)/temper
         fsmg = smg^2*(1.d0-exp(0.5*smg^2)*smg*sqrt(0.5*!pi)*erfc(smg/sqrt(2.d0))   )

        ; coefficients
         kf1= sigma1(i,j) * ui ;;; sound(i,j,k)
         kf2= exp(-ED/temper)*sqrt( 3.d+15*ED*k_B/(!pi^2*mion ) )

         afk = kf1*fsmg * ndust(i,j,k) / kf2


        ; nimg1 -  Mg on the grain surface , nimg2 - in the gas phase
         nimg1(i,j,k)= afk/(1.+afk) * metals*ngas(i,j,k)/(1.+2.*0.0851)
         nimg2(i,j,k)= 1.d0 /(1.+afk) * metals*ngas(i,j,k)/(1.+2.*0.0851)

         ; guess the (maximum) ionization fraction, as if Mg would dominate it by far
          ni_max(i,j,k)=sqrt(ioe(i,j,k)*ngas(i,j,k)/beta11  )

         temp1=nimg2(i,j,k)                ;  Mg in gas phase, ion+neutral
         temp2=9./10.*ni_max(i,j,k)   ;;;(nimg2(i,j,k)-ni_max(i,j,k)) ;   Mg-ion separated from Mg in gas phase 
         tempo=[temp1,temp2]
        nimg(i,j,k)=min(tempo)       ; define the amount of ionized Mg, it is either equal or less then nimg2


         temp1=1./10.* ni_max(i,j,k)
         temp2=ni_max(i,j,k)-nimg(i,j,k)
         tempo=[temp1,temp2]
        nh2(i,j,k)=max(tempo) ; simply take the rest for hco+

        ;;;; too much for now!
        ;;;; from reduced Eq. 16
        ;;;;nih2(i,j,k)=ni_max(i,j,k)^2*beta11/nimg(i,j,k)/alpha

        ; first rough correction
        betat1(i,j,k)=exp($
        (alog(beta11)*(nimg(i,j,k)) + alog(beta22)*(nh2(i,j,k)))/(nh2(i,j,k)+nimg(i,j,k) ) $
                    )
        mion1(i,j,k)=(mmet*nimg(i,j,k) + mhco*nh2(i,j,k))/(nh2(i,j,k)+nimg(i,j,k))

endfor
endfor
endfor

;test
;print, 'gas-phase, total Mg',  nimg2(*,jn/2,kn/2)/ngas(*,jn/2,kn/2), (nimg2(*,jn/2,kn/2)+nimg1(*,jn/2,kn/2))/ngas(*,jn/2,kn/2)
;print, '-----'
;print, 'Mg+ part, NCO+ part, total io rate',  nimg(*,jn/2,kn/2)/ngas(*,jn/2,kn/2), nh2(*,jn/2,kn/2)/ngas(*,jn/2,kn/2), $
;                      ni_max(*,jn/2,kn/2)/ngas(*,jn/2,kn/2)

print, 'INITIATE Mg ions finished'

end
