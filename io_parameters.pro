
pro io_parameters, pgrid,pdust,pdisk


         dcnt=30

         temp0=196 ; K

         G   =  6.6726d-8
         Msun = 2.d+33
         ua=(1.496d+13) ; cm
         uv= sqrt(G*Msun /ua)
         mp=1.67262e-24
         meanw= 2.3333
         k_B = 1.3806d-16 ; ;! in cm^2 g s-2 K-1
         N_A = 6.0d+23
         ioern = 7.d-19 *(f_dg/1.d-2) ;! * 86400.d0 to translate s-1 in yr-1
         ava = 20.d-5 ;! in cm
         sigma = !pi* ava^2 * 1.d0 ;! (see after Eq.51)
        ava_0= 1.d-5 ;! in cm
        ava_si=ava_0
         Numm=(ava/ava_0)^2

        sigma0=!pi*ava_0^2
         si = 1.d0
         se = 0.3d0
        dust0= 1.4d0  ; in g cm^-3
        dust_si=3.3d0
        dust_h2o=0.92d0
;
; m^+ + e^- : alpha=3.10^-6 /sqrt(T)
; M^+ + e^- : beta=3.10^-11 /sqrt(T)
; m^+ + M   : gamma=3.10^-9           ; cm^3 s^-1
;
        beta1= 3.d-11   ; Ilgner & Nelson 2006a  beta= 3.d-6 (T/300)^{-1/2}    ;8.7d-11 ;1.d-7 ;8.7d-11 ;
        tmp=1.d0 ;  300.
        elk = 4.803d-10   ; statColoumb
        aelk= 1.60217649*10.0d-19    ; absolute elektron charge
        light=2.99792458d+10 ;! cm/s
; Okuzumi's receipt for Xrays
        Lxr = 5.0d0 ;! in 10.d29 erg s-1
        ioecr0 = 1.d-17 ; 1.d-17 ;  5.d-18 ;! * 86400.d0  s-1 in yr-1
        ioexr0 = 2.6d-15 ;! * 86400.d0  s-1 in yr-1
        depcr = 96.d0 ;! g cm-2
        depxr =  8.d0 ;! g cm-2
; for 3 keV
          Lxr = 5.0d0 ;! in 1.*10.d29 erg s-1
          ioexr02 = 1.d-15 ;
          ioexr01 = 6.d-12 ; s^-1, after Bai & Goodmann 2009
           depxr1=1.5d+21 ; cm^-2 ; or 0.0064327356 g cm^-2
           depxr2=7.d+23  ; cm^-2 ; or  3.0019433   g cm^-2
              alp1= 0.4
              bet1=0.65
;   for 5 keV
;          Lxr = 1.0d0 ;! in 1.*10.d29 erg s-1  ??????????
          ioexr02b = 2.d-15 ;
          ioexr01b = 4.d-12 ; s^-1, after Bai & Goodmann 2009
           depxr1b=3.d+21 ; cm^-2
           depxr2b=1.d+24  ; cm^-2
              alp1b= 0.5
              bet1b= 0.7
;
        mum = meanw * mp ;  /N_A    ;2.34d0*1.00794/6.022d+24
        mud = (4.*!pi/3.)*(ava^2*ava_0)*dust_si ;!!! (ava^2*ava_0)=ava0^3*N !!! in g(see after Eq.46)
        mud_si=mud
        mumk= mum/k_B ; 2.34d0*1.00794/6.022d+24/k_B  ;!0.6950356    ! mass* mu/k, k in cm-1/K
        me = 9.109d-28 ;! in gramms
        critg=1.9*10.d-9/(30.+1.23)/(1./N_A) ; ! see Sano & Stone 2002a
        metals= 6.78e-07                ;7.97d-5*1.d-7
        mmet=24.3050*mp
        mhco=29.*mp
       ueta=light^2/(4.*!pi)*me/elk^2*8.28d-10
;
; summ of gas-phase abundancies, H+He+C+N+O+Si+S, see Ilgner & Nelson 2006 (I)

       abun1=meanw*mp*(1.d0+9.75*10.^(-2)+3.26*10.^(-4)+1.12*10.^(-4)+8.53*10.^(-4)+3.58*10.^(-5)+1.85*10.^(-5))



ciso=0.033333333; 498735
pfak=0.9 ;1.5; 0.5
shear=1.5
plasma=10000.

ciso1=ciso
tiny=1.d-99
;
rin=0.1
rout=300.
rin1=0.2
rout1=200.
;
Mdisk=0.064 *Msun

rcut=40.



end
