# IO

Sounds like "Io the Jupiter's moon", but it is not about the moon. It is all about ionization.
Developer: Natalia Dzyurkevich.
For those who interested: use are free to use it, please do not forget to cite Dzyurkevich et al. 2013.

# Introduction

This compact package (in IDL) provides a numerical tool to create a 2D (or 3D, if needed) map of ionization state of the gas. It is constructed following the Okuzumi et al. 2009 mathematical model. Important assumption is that the disk around the young star is filled not only with gas, but is also "dusty". The dust is assumed to be highly porous,  thus very capable to load negative charges on its surface.

# Construction

Main file is "io_code.pro", it will call functions to construct the grid, gas density and temperature profiles, to calculate penetration depth for ionizing radiation (X-rays, cosmic rays, UV), iteratively search for dominating ion (Mg+ or HCO+, depending on proximity to the star), implement Okuzumi 2009 solution for density in main ion, density of electrons, and density of negatively charged dust. 
Charge conservation is ensured. 

Function in io_diffus.pro will proceed to calculate conductivities (Ohmic, Hall, Pedersen), and to construction of 2D(3D) maps of Ohmic, ambipolar and Hall magnetic diffusivities. 

Function io_fdg.pro allows to manipulate the dust properties: assume pure silicates, or silicated covered in ice, or water evaporation line. 

# Examples of use
You need to have an IDL licence, it is often available in large research institutes.

Start IDL by typing "idl" in a command line.

IDL> .rnew io_code.pro

IDL> io_code 

# Project status

It is currently converted to python by T. Delage (MPIA), in collaboration with P. Pinilla (MPIA).
