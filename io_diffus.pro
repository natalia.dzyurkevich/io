;;
; Using: formulation of Wardle '07 for low ionization limit for multi-fluid plasma
;
; output: three elsasser numbers
;         O,A,H diffusivities
;
pro io_diffus, f_dg_t, ngas, dcharge, ndst1, mud1,sigma1, sound, omega1, mion1, ioefr, ionfr, plasma,  $
              sigmao,sigmap,sigmah, hatso,hatsp,hatsh, etao, etaa, etah, elsas_o, elsas_a, elsas_h

         ua=(1.496d+13) ; cm
         me=9.109d-28 ;! in gramms
         mp=1.67262e-24
         meanw= 2.3333
         mum=mp*meanw
         k_B = 1.3806d-16 ; ;! in cm^2 g s-2 K-1
         N_A = 6.0d+23
         elk = 4.803d-10   ; statColoumb
         aelk= 1.60217649*10.0d-19    ; absolute elektron charge
         light=2.99792458d+10 ;! cm/s
         critg=1.9*10.d-9/(30.+1.23)/(1./N_A) ; ! see Sano & Stone 2002a
         si = 1.d0
         se = 0.3d0
         tiny=1.d-99


; recover the size
szds=size(ndst1)
in=szds(1) & jn=szds(2) & kn=szds(3)  & dcnt=szds(4)-1
print, in,jn,kn,dcnt

; ------- define 

ambip=dblarr(in,jn,kn) & hallt=dblarr(in,jn,kn)

etao=dblarr(in,jn,kn) &  etaa=dblarr(in,jn,kn) & etah=dblarr(in,jn,kn) 
hatso=dblarr(in,jn,kn,3) & hatsp=dblarr(in,jn,kn,3) & hatsh=dblarr(in,jn,kn,3) 
sigmao=dblarr(in,jn,kn) & sigmap=dblarr(in,jn,kn) & sigmah=dblarr(in,jn,kn)
elsas_o=dblarr(in,jn,kn) & elsas_a=dblarr(in,jn,kn) & elsas_h=dblarr(in,jn,kn) 
;-----------calculate------------------------------

;test
;print, ioefr(*,jn/2,kn/2), ionfr(*,jn/2,kn/2)


           for i=0,in-1 do begin
            for k=0,kn-1 do begin
             for j=0,jn-1 do begin

            ui=sqrt(8.*sound(i,j,k)^2*mum/!Pi/mion1(i,j,k))
            ue=ui*sqrt(mion1(i,j,k)/me)   ;! in cm /s   ; assumption T_e=T_i
            temper=sound(i,j,k)^2*meanw/(N_A*k_B)


;c----------
;c----------
;   dv=dx1a*dx2a*dx3a*sin(x2b(j))*x1b(i)^3*ua^3
;c----------
;c
; changed ioefr to ionfr!
;         deta(i,j,k)=234.d0* $
;           sqrt( sound(i,j,k)^2*meanw*2./(N_A*k_B) ) / (ionfr(i,j,k)/(d(i,j,k)*d0/mum)  ) ;/uv/ua

;         seta(i,j,k)=234.d0* $
;           sqrt(sound(i,j,k)^2*meanw*2./(N_A*k_B))/ioend(i,j,k);/uv/ua
;
;         seta2(i,j,k)=234.d0* $
;                    sqrt(sound(i,j,k)^2*meanw*2./(N_A*k_B))/ioend2(i,j,k)
;         seta3(i,j,k)=234.d0* $
;                sqrt(sound(i,j,k)^2*meanw/(N_A*k_B))/ioend3(i,j,k)
;
;         critg=1.9d-9/(30.*mum+1.23*mum)
; ambip and hallt are I/A and I/H dimensionless criteria, not terms themself
         ambip(i,j,k)=ionfr(i,j,k)*critg*mion1(i,j,k)/omega1(i,j)
         hallt(i,j,k)=ioefr(i,j,k) *4.*!pi* elk *sound(i,j,k)^2/1000. $
           /( light*omega1(i,j)*sqrt(4.*!pi*ngas(i,j,k)*mum    *sound(i,j,k)^2/1000.) )


;========================================================================
; new treatment of ambipolar diffusion - see Nakano & Umebayashi 1986
;========================================================================
; 1 = elektron, 2 = ion, 3 = dust negatively charged

     bfield= sqrt( 8.*!pi*sound(i,jn/2,kn/2)^2*ngas(i,jn/2,kn/2)*mum / plasma  )
;

q1=-1. & q2= 1. & q3=dcharge(i,j,k)

;if (NoDust eq 1) then  begin
;; TEST !!!
;  ionfr(i,j,k)= d(i,j,k)*d0/mum *sqrt(ioe(i,j,k)/(d(i,j,k)*d0/mum ) /(betat1(i,j,k)  )   )
;  ioefr(i,j,k)  = ionfr(i,j,k) & q3=0.0
;endif

lighti=light^(-1)

onu1 = abs(q1) * elk *  bfield *lighti /me
onu2 = abs(q2) * elk *  bfield * lighti /mion1(i,j,k)
onu3 = abs(q3) * elk *  bfield *lighti /mud1(i,j)
;
sigmave = 1.d-15 * sqrt( 128.*k_B*temper/(9.*!pi*me) )    ;8.28d-10*sqrt(temper/300.) ;&
sigmavi = 1.9d-9 
sigmavd=sigma1(i,j)*sqrt(16.*k_B*temper/(!pi*mud1(i,j)) $
                   +128./9.*k_B*temper/(!pi*mum)) 
;
collf1= ( mum+ me  )*  ( ngas(i,j,k)*mum * sigmave )^(-1)
collf2=  ( mum+ mion1(i,j,k))*  ( ngas(i,j,k)*mum * sigmavi )^(-1)
collf3=  ( mum+ mud1(i,j) )*  ( ngas(i,j,k)*mum * sigmavd )^(-1)
;
fakb1=onu1*collf1
fakb2=onu2*collf2
fakb3=onu3*collf3
;
;wbi(i,j,k)=fakb2
;wbd(i,j,k)=collf3*abs(q3) * elk *  bfield *lighti /mud
;
copl1=1.d0/(1.d0+fakb1^2) & copl2=1.d0/(1.d0+fakb2^2) &  copl3=1.d0/(1.d0+fakb3^2)
;
;
hatso(i,j,k,0)= fakb1*abs(q1)*ioefr(i,j,k) * elk*light/ bfield
hatso(i,j,k,1)= fakb2*abs(q2)*ionfr(i,j,k) * elk*light/ bfield
hatso(i,j,k,2)= 0.0;

;hatsh(i,j,k,0)= q1 *ioefr(i,j,k)*copl1 * elk*light/ bfield   *fakb1^2
;hatsh(i,j,k,1)= q2 *ionfr(i,j,k)*copl2 * elk*light/ bfield   *fakb2^2
;hatsh(i,j,k,2)= 0.0;

; change to well-coupled limit !!! for neal
if (fakb2 gt 1.d5) then begin
fneal1=1.d0
endif else begin
fneal1=- fakb1^2
endelse
if (fakb2 gt 1.d5) then begin
fneal2=1.d0
endif else begin
fneal2=- fakb2^2
endelse
hatsh(i,j,k,0)= - q1 *ioefr(i,j,k)* elk*light/ bfield  * copl1*fneal1
hatsh(i,j,k,1)= - q2 *ionfr(i,j,k)* elk*light/ bfield  * copl2*fneal2
hatsh(i,j,k,2)= 0.0

hatsp(i,j,k,0)=fakb1*abs(q1)*ioefr(i,j,k)*copl1  * elk*light/ bfield
hatsp(i,j,k,1)=fakb2*abs(q2)*ionfr(i,j,k)*copl2  * elk*light/ bfield
hatsp(i,j,k,2)=0.0;
;

; ---------make dust conductivity -----------
hatso(i,j,k,2)=0.0 & hatsh(i,j,k,2)=0.0 & hatsp(i,j,k,2)=0.0

for m1=0,dcnt do begin
q3d=float(-dcnt/2+m1)+dcharge(i,j,k)
onu3d = abs(q3d) * elk *  bfield *lighti /mud1(i,j)
fakb3d=onu3d*collf3
copl3d=1.d0/(1.d0+fakb3d^2)
if (fakb2 gt 1.d5) then begin
fneal3=1.d0
endif else begin
fneal3= - fakb3d^2
endelse
;
hatso(i,j,k,2)=hatso(i,j,k,2)+fakb3d*abs(q3d)*ndst1(i,j,k,m1) * elk*light/ bfield
;;;;hatsh(i,j,k,2)=hatsh(i,j,k,2)+q3d *ndst1(i,j,k,m1)*copl3d * elk*light/ bfield   *fakb3d^2
hatsp(i,j,k,2)=hatsp(i,j,k,2)+fakb3d*abs(q3d)*ndst1(i,j,k,m1)*copl3d  * elk*light/ bfield
; change to well-coupled limit !!! for neal
hatsh(i,j,k,2)=hatsh(i,j,k,2) - q3d *ndst1(i,j,k,m1)*copl3d * elk*light/ bfield * fneal3
;------------------------------------------
;; for neal
;if ( i eq 2 and k eq 2) then begin
;;  write conductivities
;forneal(0,2+m1,j)= fakb3d*abs(q3d)*ndst1(i,j,k,m1) * elk*light/ bfield
;forneal(1,2+m1,j) = - q3d *ndst1(i,j,k,m1)*copl3d * elk*light/ bfield ;;;;  *fakb3d^2
;forneal(2,2+m1,j)= fakb3d*abs(q3d)*ndst1(i,j,k,m1)*copl3d  * elk*light/ bfield
;;write charge
;forneal(3,2+m1,j)=q3d
;; write densities
;forneal(4,2+m1,j)=ndst1(i,j,k,m1)
;;write beta
;forneal(5,2+m1,j)=fakb3d
;endif

endfor
;

; make total conductivities------------------

sigmao(i,j,k)=  (hatso(i,j,k,0)+hatso(i,j,k,1)   +hatso(i,j,k,2)  )
sigmah(i,j,k)= - ( hatsh(i,j,k,0)+hatsh(i,j,k,1)+hatsh(i,j,k,2)  )
sigmap(i,j,k)= ( hatsp(i,j,k,0)+hatsp(i,j,k,1)  +hatsp(i,j,k,2)  )

;-----------diffusivity---------------------

etao(i,j,k)=light^2/(4.*!pi*sigmao(i,j,k) +tiny )
;
etah(i,j,k)=light^2/(4.*!pi*(sigmah(i,j,k)^2+sigmap(i,j,k)^2) +tiny )*sigmah(i,j,k)
;
sigmaper=( sigmah(i,j,k)^2+sigmap(i,j,k)^2 +tiny)^(-1)

etaa(i,j,k)=light^2/(4.*!pi)*( $
  sigmap(i,j,k)/( sigmah(i,j,k)^2+sigmap(i,j,k)^2 )  $
  - 1.d0/sigmao(i,j,k) )

; experiment ; remove dust contribution to conductivity
; O/I
height= sound(i,j,k)/omega1(i,j)    ;x1b(i)*ua*ciso
;-------------------------------------------------------
; icrit is a estimated 'weight' of inductive term, (height*sound(i,j,k) ) in Wardle,
;                                                  *omega1(i,j) * 4.*!pi*d(i,j,k)*d0  /  bfield^2 in ASano
;
;  delsas(i,j,k)=  etao(i,j,k) /(height*sound(i,j,k) );  *omega1(i,j) * 4.*!pi*d(i,j,k)*d0  /  bfield^2
  delsasi = etao(i,j,k)  *omega1(i,j) * 4.*!pi*ngas(i,j,k)* mum  /  bfield^2
  elsas_o(i,j,k)=1.d0/(delsasi + tiny)
;-------------
; A/O * delsas = A/I
;
;ambip(i,j,k)= etaa(i,j,k)/(height*sound(i,j,k) );   *omega1(i,j) * 4.*!pi*d(i,j,k)*d0  /  bfield^2
ambipi = etaa(i,j,k)  *omega1(i,j) * 4.*!pi*ngas(i,j,k)*mum  /  bfield^2

elsas_a(i,j,k)=1.d0/(ambipi + tiny)

; H/O *delsas = H/I
;
;hallt(i,j,k)= etah(i,j,k)/(height*sound(i,j,k) );   *omega1(i,j) * 4.*!pi*d(i,j,k)*d0  /  bfield^2     ;;;;;/etao(i,j,k) * delsas(i,j,k)
hallti = etah(i,j,k) *omega1(i,j) * 4.*!pi*ngas(i,j,k)* mum  /  bfield^2

elsas_h(i,j,k)=1.d0/( hallti + tiny)

            endfor
           endfor
          endfor
;

;
;
;for i=0,in-1 do begin
;print, i, x1b(i),x2b(jn/2),ehao(i,jn/2,kn/2),ehaa(i,jn/2,kn/2),ehah(i,jn/2,kn/2) 
;endfor

print, 'FINISHED making of diffusivities and elsasser numbers'

end


