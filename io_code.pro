;
;-----------------------------------------------------------------------
;An Atlas of Dead Zones in Protostellar Discs
;
;Parameters to vary (fiducial value):
;  stellar mass (1 Msun)                                                        +
;  disk mass (0.03 Mstar ~3x MMSN)                                              +
;  surface density power law index (p=-1)                                       +  
;  dust-to-gas mass ratio; dust well-mixed (10^-4)                              +
;  representative monomer size (0.1 um; aggregates N>=400)                      +
;  X-ray luminosity (2x10^30 erg/s)                                             +
;  magnetic field power law index (-11/8 = (p-1.5+q/2)/2)                       +
;  magnetic field normalization (midplane beta 400 at 100 AU)                   +?
;
;With the constraints that the model disks must
;  be at most marginally gravitationally unstable: Q>~1
;  be optically-thick to the starlight so L_disk/L_star > 0.01
;  have detectable millimeter flux
;  be opaque at visible wavelengths out to at least 10 AU
;
; MODIFICATION:: intro the snow line after OKa, Nakamoto & Ida 2011 ApJ 738,141
;   dust consists of 4.3d-3 of silicates and 9.4d-3 of water ice.
;   Internal densities are dust_si=3.3 g/cm^3 and dust_h2o=0.92g/cm^3.
;
;------------------------------------------------------------------------
;
;
pro io_code


verbose=0
save_plot=1

;    PROBLEM SIZE 

    in=200 & jn=80 & kn=6
    xlogscale=1
;
;   grid parameters
;

ciso=0.033333333; 498735
Hscale=12.  ;; last two give vertical extent

;test
rin=0.1
rout=200.

;rin=0.1
;rout=300.   ;; radial extent

;
;    DISK parameters
;
pfak=0.9 
shear=1.5
Msun=2.d+33 ; solar mass in g
Mdisk=0.064 *Msun
rcut=40.
temp0= 196. ;K
;
;    DUST parameters
;
     fidu =2
     dcnt=30
     numb_iter=10
;
;    magnetic field
;

     plasma=4.d2     ;  10000.

;
;    diverse gas parameters
;
         metals=1.d-8  ; n[Mg]/n[H] relative abundance
         
         G   =  6.6726d-8
         unit_m = 2.d+33
         ua=(1.496d+13) ; cm
         uv= sqrt(G*Msun /ua)
         mp=1.67262e-24
         meanw= 2.3333
         mum=mp*meanw
         k_B = 1.3806d-16 ; ;! in cm^2 g s-2 K-1
         N_A = 6.0d+23
         ava = 20.d-5 ;! in cm
         sigma = !pi* ava^2 * 1.d0 ;! (see after Eq.51)
         ava_0= 1.d-5 ;! in cm
         ava_si=ava_0
         Numm=(ava/ava_0)^2
          dust0= 1.4d0
;
;    counter
;
   aaa=31

    kkk=0
    nl=2
   monom=dblarr(nl)
   monom=[2.,4.] ;;;3.,4.,5.,6.]
;
;
;
;===================1================make disk========
;
;
;

xlogscale=1

  io_do_grid, in,jn,kn,rin,rout,Hscale, xlogscale, dx1a,dx2a,x1b,x2b
           js = 2
           je = jn -3
           is = 2
           ie = in -3
           ks = 2
           ke = kn -3

;------------------------------------------------

;;NEW
  io_do_disk,in,jn,kn,x1b,x2b,rin,temp0, Mdisk,pfak,rcut, $
             ngas, sound,verzh,omega1



;--------------------------------------LOOP--------------------------
;
 print, 'START LOOP OVER MONOM', nl
; 
;

   for kkk=0, nl-1 do begin
;
         aaa=31+kkk

         ppp=monom(kkk)

NoDust=0
;
if NoDust eq 1 then begin
plab='NoDust'
endif
if NoDust eq 0 then begin
plab='1.e-'+'D'+strcompress(string(ppp, FORMAT='(I3)'),/remove_all)   ;  strcompress(string(f_dg,FORMAT='(F8.6)'),/remove_all)
endif
print, '170: plab=',plab


;
bbb=dblarr(in,jn)


;
  tstop=dblarr(jn,in)
  tsettl=dblarr(jn,in)
  tsettl1=dblarr(jn,in,6)

snow=dblarr(in,jn) & snow(*,*)=0.d0
metal=dblarr(in,jn) & metal(*,*)=0.d9


Psat=0.d0

;--------------SNOW and METAL-------LOOP over DISK-------------

for i=is-2,ie+2 do begin
for  k=ks-2,ke+2 do begin
for j=js-2,je+2 do begin
;
; create bbb, snow and metal
;
temper=sound(i,j,k)^2*meanw/(N_A*k_B)
;forneal2(i,j)=(x2b(j)-!pi/2.)/verzh(i,j)
bbb(i,j)=(x2b(j)-!pi/2.)/verzh(i,j)
;
pressure=ngas(i,j,k)*mum*sound(i,j,k)^2* 1.2d-3
;
Psat=exp(-6070.d0/temper +30.86) 
;continue...
if (pressure lt Psat) then begin
snow(i,j)=0.d0
endif else begin
snow(i,j)=1.d0
endelse

;metal line
t100=98.d0
t50=44.d0
nkk=0.18
funk=1.d0;;;(sin(x2b(j)))^(+0.5)
dfunk=(ngas(i,j,k)/ngas(i,jn/2,k))^(nkk)
tcrit=funk*(t100+t50*dfunk/(alog10( 10.^(-2)/10.^(-ppp)+9.)))
if (temper lt tcrit ) then begin
metal(i,j)=0.d0
endif else begin
metal(i,j)=1.d0
endelse

;
tstop(j,i) = sqrt(3.14/8.)*ava_0*dust0/mum/ngas(i,j,k)/sound(i,j,k)*omega1(i,j)  ; normierte t_stopping
tsettl(j,i)=(tstop(j,i)  )^(-1); ! in units of Omega
tsettl1(j,i,0)=1./(sqrt(3.14/8.)*1.d-4*dust0/mum/ngas(i,j,k)/sound(i,j,k)*omega1(i,j))
tsettl1(j,i,1)=1./(sqrt(3.14/8.)*1.d-3*dust0/mum/ngas(i,j,k)/sound(i,j,k)*omega1(i,j))
tsettl1(j,i,2)=1./(sqrt(3.14/8.)*1.d-2*dust0/mum/ngas(i,j,k)/sound(i,j,k)*omega1(i,j))
tsettl1(j,i,3)=1./(sqrt(3.14/8.)*1.d-1*dust0/mum/ngas(i,j,k)/sound(i,j,k)*omega1(i,j))
tsettl1(j,i,4)=1./(sqrt(3.14/8.)*1.d0*dust0/mum/ngas(i,j,k)/sound(i,j,k)*omega1(i,j))

endfor
endfor
endfor
;--------------------------------Disk FINISHED


  io_do_fdg, ppp,x1b,x2b,snow, ngas , $ 
              f_dg_t,ndust, mud1, ava1, sigma1, fidu

;print, ndust(in/2,*,kn/2), mud1(in/2,*), sigma1(in/2,*)

;--------------------------------Dust FINISHED
;
;forneal3(1)=ngas(2,jn/2-34,kn/2)*mum
;forneal3(8)=omega1(2,jn/2-34)
;;

if (verbose eq 1) then begin
;  magfield0 = sqrt( sound(49,jn/2,kn/2)^2 * mum*ngas(49,jn/2,kn/2) *8.0 * !pi / plasma )
  magfield0= sqrt( sound(1,jn/2,kn/2)^2 * mum*ngas(1,jn/2,kn/2) *d0  *8.0 * !pi / plasma )
  print, '433:initial magnetic field!', magfield0
  print, '434:',sqrt( sound(in/2,jn/2,kn/2)^2 * mum*ngas(in/2,jn/2,kn/2)   *8.0 * !pi / plasma ) 
  print, '435',sqrt( sound(in-1,jn/2,kn/2)^2 * mum*ngas(in-1,jn/2,kn/2)   *8.0 * !pi / plasma )
  print, '437:saturated pressure Psat=', exp(-6070.d0/temper +30.86)
  print, '438:snow at the midplane', snow(*,jn/2)
endif ; verbose
;
;
;-----------------------------calcullate the surface density and ionization rate------------
;


    io_rates, x1b,x2b,ngas,f_dg_t,$
             ioe, writefile=aaa

;   print, ioe(in/2,*,kn/2)
;
;------------------------------guess initial Mg in gas phase----------
;

    io_Mg_init, metals, sound, ioe, ngas, ndust, sigma1, $
                betat1, mion1, nimg,nimg2,nh2

 ; print, ioe(*,jn/2,kn/2)


;-------------------------------Solve Iteratively Okuzumi's Eqs for n_ion, n_elc, dust charge... together with Mg------------------

;
;
  io_oku_iter_Mg, metals, numb_iter, dcnt, ioe, ngas, sound, ndust, sigma1, $
                            ava1, nimg, nimg2,nh2,   $
                    ioefr, ionfr, ndst1, dcharge, gfunk1, mion1 ;;;;; dchag1, dchargv, ndst1

 ;; print, ioefr(in/2,*,kn/2), ionfr(in/2,*,kn/2), ndst1(in/2,*,kn/2), dcharge(in/2,*,kn/2)
 ;;   print, 'Check ndst1::', ndst1(in/2,*,kn/2,*)
     print, 'number of ions', ionfr(*,jn/2,kn/2)

;
; -----------------------------make currents and magnetic diffusivities, O, A, H -------------------------------
;

 io_diffus, f_dg_t, ngas,dcharge, ndst1, mud1, sigma1, sound, omega1, mion1, ioefr, ionfr, plasma, $
            sigmao,sigmap,sigmah, hatso,hatsp,hatsh, etao, etaa, etah, elsas_o, elsas_a, elsas_h

;
;for i=0,in-1 do begin
;print, i, x1b(i),x2b(jn/2),hatso(i,jn/2,kn/2,2),hatsp(i,jn/2,kn/2,2),hatsh(i,jn/2,kn/2,2)
;endfor



;;;;;;;;;;;;;;;;;;;;;;;; PLOTTING:::::::::::::::::::::::::::::::::::::::::::::

;------------------------conductivities------------
;
;
set_plot,'ps'
device,filename='TermsS'+plab+strcompress(string(aaa),/remove_all)+'.ps',ysize=22,xsize=22, /color;,yoffset=2
!p.charsize=2.0
!P.THICK=2
!x.thick=2
!p.charthick=2
!y.thick=2
!p.multi=[0,3,3]
begin
loadct, 6
bnm=1.d0 ;
;
rn=0
;
;[1]
plot, (x2b(*)-!pi/2.d0)/verzh(rn,*),  alog10(hatso(rn,*,kn/2,1)/sigmao(rn,*,kn/2)), line=0, xrange=[-8.,8.], xstyle=1,   $
title='Ohmic,'+string(x1b(rn),Format='(F3.1)')+'AU', $
ytitle=textoidl('\sigma_{o,e}, \sigma_{o,i}, \sigma_{o,dust} [s^{-1}]'), xtitle='z/H',  yrange=[-10.,2.], ystyle=1
oplot, (x2b(*)-!pi/2.d0)/verzh(rn,*),  alog10(hatso(rn,*,kn/2,0)/sigmao(rn,*,kn/2)), line=1
oplot, (x2b(*)-!pi/2.d0)/verzh(rn,*),  alog10(hatso(rn,*,kn/2,2)/sigmao(rn,*,kn/2)), line=3
;
;charsize=1.2
;oplot, [-3.,-1.], [9.,9.], color=185
;oplot, [-3.,-1.], [10.,10.], color=85
;oplot, [-4.,-1.], [9.,9.], color=55, line=2
;xyouts, -1.,7., textoidl(' \sigma_{o}')
;xyouts, -1.,9., textoidl(' \sigma_{P}')
;xyouts, -1.,9., textoidl(' no dust'), charsize=1.
;
;[2]
plot, (x2b(*)-!pi/2.d0)/verzh(rn,*),  alog10(abs(hatsp(rn,*,kn/2,0))/sigmap(rn,*,kn/2)), line=1,  $
title= 'Petersen,'+string(x1b(rn),Format='(F3.1)')+'AU',  xrange=[-8.,8.], xstyle=1,  yrange=[-10.,2.], ystyle=1, $
ytitle=textoidl('\sigma_{P,e}, \sigma_{P,i}, \sigma_{P,dust} [s^{-1}]'), xtitle='z/H'
oplot, (x2b(*)-!pi/2.d0)/verzh(rn,*),  alog10(abs(hatsp(rn,*,kn/2,1))/sigmap(rn,*,kn/2)), line=2
oplot, (x2b(*)-!pi/2.d0)/verzh(rn,*),  alog10(abs(hatsp(rn,*,kn/2,2))/sigmap(rn,*,kn/2)), line=3
;

;charsize=1.2
;oplot, [-7.,-5.],[6.,6.],line=0, color=85
;xyouts, -5., 6., 'total'
;oplot, [-7.,-5.],[7.,7.],line=1
;xyouts, -5., 7., textoidl('\sigma_{e}')
;oplot, [-7.,-5.],[8.,8.],line=2
;xyouts, -5., 8., textoidl('\sigma_{i}')
;oplot, [-7.,-5.],[9.,9.],line=3
;xyouts, -5., 9., textoidl('\sigma_{dust}')

;[3]
plot, (x2b(*)-!pi/2.d0)/verzh(rn,*),  alog10(abs(hatsh(rn,*,kn/2,0))/sigmah(rn,*,kn/2)), line=1 , $
title='Hall,'+string(x1b(rn),Format='(F3.1)')+'AU', xrange=[-8.,8.], xstyle=1, yrange=[-10.,2.], $
 ystyle=1, ytitle=textoidl('\sigma_{H,e}, \sigma_{H,i}, \sigma_{H,dust} [s^{-1}]'), xtitle='z/H'
;
oplot, (x2b(*)-!pi/2.d0)/verzh(rn,*),  alog10(abs(hatsh(rn,*,kn/2,1)/sigmah(rn,*,kn/2))), line=2
oplot, (x2b(*)-!pi/2.d0)/verzh(rn,*),  alog10(abs(hatsh(rn,*,kn/2,2)/sigmah(rn,*,kn/2))), line=3
;
;
;charsize=1.2
;oplot, [-7.,-5.],[6.,6.],line=0, color=115
;xyouts, -5., 6., 'total'
;oplot, [-7.,-5.],[7.,7.],line=1
;xyouts, -5., 7., textoidl('\sigma_{e}')
;oplot, [-7.,-5.],[8.,8.],line=2
;xyouts, -5., 8., textoidl('\sigma_{i}')
;oplot, [-7.,-5.],[9.,9.],line=3
;xyouts, -5., 9., textoidl('\sigma_{dust}')
;;a
;
rn=40/10
;
plot, (x2b(*)-!pi/2.d0)/verzh(rn,*),  alog10(hatso(rn,*,kn/2,1)/sigmao(rn,*,kn/2)), line=2, xrange=[-8.,8.], xstyle=1 ,  $
title='Ohmic,'+string(x1b(rn),Format='(F3.1)')+'AU', $
ytitle=textoidl('\sigma_{o,e}, \sigma_{o,i}, \sigma_{o,dust} [s^{-1}]'), xtitle='z/H',  yrange=[-10.,2.], ystyle=1
oplot, (x2b(*)-!pi/2.d0)/verzh(rn,*),  alog10(hatso(rn,*,kn/2,0)/sigmao(rn,*,kn/2)), line=1
oplot, (x2b(*)-!pi/2.d0)/verzh(rn,*),  alog10(hatso(rn,*,kn/2,2)/sigmao(rn,*,kn/2)), line=3
;
plot, (x2b(*)-!pi/2.d0)/verzh(rn,*),  alog10(abs(hatsp(rn,*,kn/2,0))/sigmap(rn,*,kn/2)), line=1,  $
title= 'Petersen,'+string(x1b(rn),Format='(F3.1)')+'AU',  xrange=[-8.,8.], xstyle=1,  yrange=[-10.,2.], ystyle=1, $
ytitle=textoidl('\sigma_{P,e}, \sigma_{P,i}, \sigma_{P,dust} [s^{-1}]'), xtitle='z/H'
oplot, (x2b(*)-!pi/2.d0)/verzh(rn,*),  alog10(abs(hatsp(rn,*,kn/2,1))/sigmap(rn,*,kn/2)), line=2
oplot, (x2b(*)-!pi/2.d0)/verzh(rn,*),  alog10(abs(hatsp(rn,*,kn/2,2))/sigmap(rn,*,kn/2)), line=3
;
plot, (x2b(*)-!pi/2.d0)/verzh(rn,*),  alog10(abs(hatsh(rn,*,kn/2,0)/sigmah(rn,*,kn/2))), line=1 , $
title='Hall,'+string(x1b(rn),Format='(F3.1)')+'AU', xrange=[-8.,8.], xstyle=1, yrange=[-10.,2.], $
 ystyle=1, ytitle=textoidl('\sigma_{H,e}, \sigma_{H,i}, \sigma_{H,dust} [s^{-1}]'), xtitle='z/H'
;
oplot, (x2b(*)-!pi/2.d0)/verzh(rn,*),  alog10(abs(hatsh(rn,*,kn/2,1)/sigmah(rn,*,kn/2))), line=2
oplot, (x2b(*)-!pi/2.d0)/verzh(rn,*),  alog10(abs(hatsh(rn,*,kn/2,2)/sigmah(rn,*,kn/2))), line=3
;
rn=90/10
;
plot, (x2b(*)-!pi/2.d0)/verzh(rn,*),  alog10(hatso(rn,*,kn/2,1)/sigmao(rn,*,kn/2,0)), line=2, xrange=[-8.,8.], xstyle=1 ,  $
title='Ohmic,'+string(x1b(rn),Format='(F4.1)')+'AU', $
ytitle=textoidl('\sigma_{o,e}, \sigma_{o,i}, \sigma_{o,dust} [s^{-1}]'), xtitle='z/H',  yrange=[-10.,2.], ystyle=1
oplot, (x2b(*)-!pi/2.d0)/verzh(rn,*),  alog10(hatso(rn,*,kn/2,0)/sigmao(rn,*,kn/2,0)), line=1
oplot, (x2b(*)-!pi/2.d0)/verzh(rn,*),  alog10(hatso(rn,*,kn/2,2)/sigmao(rn,*,kn/2,0)), line=3
;
plot, (x2b(*)-!pi/2.d0)/verzh(rn,*),  alog10(abs(hatsp(rn,*,kn/2,0))/sigmap(rn,*,kn/2)), line=1,  $
title= 'Petersen,'+string(x1b(rn),Format='(F4.1)')+'AU',  xrange=[-8.,8.], xstyle=1,  yrange=[-10.,2.], ystyle=1, $
ytitle=textoidl('\sigma_{P,e}, \sigma_{P,i}, \sigma_{P,dust} [s^{-1}]'), xtitle='z/H'
oplot, (x2b(*)-!pi/2.d0)/verzh(rn,*),  alog10(abs(hatsp(rn,*,kn/2,1))/sigmap(rn,*,kn/2)), line=2
oplot, (x2b(*)-!pi/2.d0)/verzh(rn,*),  alog10(abs(hatsp(rn,*,kn/2,2))/sigmap(rn,*,kn/2)), line=3
;
plot, (x2b(*)-!pi/2.d0)/verzh(rn,*),  alog10(abs(hatsh(rn,*,kn/2,0)/sigmah(rn,*,kn/2))), line=1 , $
title='Hall,'+string(x1b(rn),Format='(F4.1)')+'AU', xrange=[-8.,8.], xstyle=1, yrange=[-10.,2.], $
 ystyle=1, ytitle=textoidl('\sigma_{H,e}, \sigma_{H,i}, \sigma_{H,dust} [s^{-1}]'), xtitle='z/H'
;
oplot, (x2b(*)-!pi/2.d0)/verzh(rn,*),  alog10(abs(hatsh(rn,*,kn/2,1)/sigmah(rn,*,kn/2))), line=2
oplot, (x2b(*)-!pi/2.d0)/verzh(rn,*),  alog10(abs(hatsh(rn,*,kn/2,2)/sigmah(rn,*,kn/2))), line=3
;
;
end
device,/close
set_plot,'x'
;

;
checkh=dblarr(jn,in)
verzhr=rotate(verzh,4)
checkh=(x2b-!pi/2.d0)/verzhr
;print, 'size checkh', size(checkh)
;
topg=!pi/2.-5.0*ciso
botg=!pi/2.+5.0*ciso
h1a=rin*cos(topg)/sin(topg) & h1b=rout*cos(topg)/sin(topg)
h2a=rin*cos(botg)/sin(botg) & h2b=rout*cos(botg)/sin(botg)
;

;-----------------------------------------[][]
;
htn=textoidl('log(\gamma\rho_i/\Omega)')
;

;-----------------------------------------[]1[]
  set_plot,'ps'
  device,filename='elsasOA20'+plab+strcompress(string(aaa),/remove_all)+'.ps',ysize=16,xsize=20, /color;,yoffset=2
    !p.charsize=1.2
    !P.THICK=2
    !x.thick=2
    !p.charthick=2
    !y.thick=2
    !p.multi=[0,1,1]
   begin
   loadct,0
;

totald=1.d0/(1.d0/elsas_o+1.d0/elsas_a)
;print, 'total', size(totald)
for i=0,in-1 do begin
temper=sound(i,jn/2,kn/2)^2*meanw/(N_A*k_B)
;print, 'total-d', i, x1b(i), temper, ngas(i,jn/2,kn/2)*mum, ndust(i,jn/2,kn/2), $
;                     elsas_o(i,jn/2,kn/2), elsas_a(i,jn/2,kn/2) ;;;  totald(*,jn/2,kn/2)
endfor
rin1= 0.2
rout1=200.

;[1]
loadct,0
th1=-10.*ciso
th2= 10.*ciso
;;
loadct,33
firstcolor=0
contour, alog10(totald(*,*,kn/2)), x1b(*), (x2b(*)-!pi/2.d0) $
,  xtitle='!6 Radius [AU]',  ytitle=textoidl('\Theta-\pi/2') ,  xrange=[rin1, rout1], xstyle=1, /xlog $
, title='!6 log(!7K!X!6) for f_dg='+ plab+' ' $
 ,zrange=[ -8., 8.], yrange=[0.,th2*0.90] $
,  ystyle=1 $
, levels=[-19.,-1.,0.0,1. ,9.  ], c_colors=[200,180, 80, 80,80 ] $  ;  ...    c_colors=[40,40,40,210,80,80,80,80 ]  $ ;  
   , /closed, /fill ;;; , /overplot
loadct,0
contour, alog10(totald(*,*,kn/2) ), x1b(*), (x2b(*)-!pi/2.d0), $
         levels=[-1.,0.0,1. ],  C_ANNOTATION = [ '-1','0',' 1'] $
, /closed, /overplot
;loadct,7
;contour, alog10(elsas_o(*,*,kn/2)),  x1b(*), (x2b(*)-!pi/2.d0), thick=1., c_colors=[40,40,40],  levels=[-1., 0.0, 1. ], $
;         C_ANNOTATION = [ '-1','0','1'], c_linestyle=[1,1,1], /overplot
loadct,0
contour,  bbb(*,*), x1b(*), (x2b(*)-!pi/2.d0), c_colors=[10,10],  levels=[-5.,-1.,1.,5. ], c_linestyle=[2,2], /overplot
;
; oplot elsass_o

;contour, alog10(elsas_o(*,*,kn/2)), x1b(*),(x2b(*)-!pi/2.), $
;         levels=[-1.,0.,1.], c_linestyle=[1,1,1], /overplot

; snow & metal lines

loadct,8
contour, metal, x1b,(x2b-!pi/2.), $
levels=[0.,1.], c_colors=[10,160], C_lines=[1,1], $
C_ANNOTATION=['e','metal line'], thick=5, /overplot

loadct,43
contour, snow, x1b,(x2b-!pi/2.), $
levels=[0.,1.], c_colors=[10,205], C_lines=[1,1], $
C_ANNOTATION=['e','snow line'], thick=5, /overplot
loadct,0

;;
;; FIND
xyouts, 15.,0.15, 'a!i0!n=0.1 !7l!6m' , charsize=1.1
xyouts, 15.,0.13, 'a =2 !7l!6m' , charsize=1.1
end
device,/close
set_plot,'x'
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;SAVE results for further plotting;;;;;;;;;;;;;;;;;;;
;
if (save_plot gt 0) then begin

;
inn=size(totald)
in1=long(41) & in1=inn(1)
jn1=long(41) & jn1=inn(2)
temp=dblarr(in,jn) & temp(*,*)=totald(*,*,kn/2)
close, 11
openw,11,'total'+plab+strcompress(string(aaa),/remove_all)+'.plot'
writeu, 11, in1
writeu, 11, jn1
writeu, 11, temp
close,11

temp(*,*)=betat1(*,*,kn/2)
close, 11
openw,11,'recbeta'+plab+strcompress(string(aaa),/remove_all)+'.plot'
writeu, 11, in1
writeu, 11, jn1
writeu, 11, temp
close,11



 temp(*,*)=mum*ngas(*,*,kn/2)
close, 12
openw,12,'densi'+plab+strcompress(string(aaa),/remove_all)+'.plot'
writeu, 12, in1
writeu, 12, jn1
writeu, 12, temp
close,12

 temp(*,*)=ioe(*,*,kn/2)
close, 12
openw,12,'ioe'+plab+strcompress(string(aaa),/remove_all)+'.plot'
writeu, 12, in1
writeu, 12, jn1
writeu, 12, temp
close,12

 temp(*,*)=ioefr(*,*,kn/2)
close, 12
openw,12,'nioe'+plab+strcompress(string(aaa),/remove_all)+'.plot'
writeu, 12, in1
writeu, 12, jn1
writeu, 12, temp
close,12

 temp(*,*)=ionfr(*,*,kn/2)
close, 12
openw,12,'nion'+plab+strcompress(string(aaa),/remove_all)+'.plot'
writeu, 12, in1
writeu, 12, jn1
writeu, 12, temp
close,12
;
;
 temp(*,*)=hatsp(*,*,kn/2,1)
close, 12
openw,12,'sigmap-ion-'+plab+strcompress(string(aaa),/remove_all)+'.plot'
writeu, 12, in1
writeu, 12, jn1
writeu, 12, temp
close,12
;
 temp(*,*)=hatsp(*,*,kn/2,2)
close, 12
openw,12,'sigmap-dst-'+plab+strcompress(string(aaa),/remove_all)+'.plot'
writeu, 12, in1
writeu, 12, jn1
writeu, 12, temp
close,12
;
 temp(*,*)=hatsp(*,*,kn/2,0)
close, 12
openw,12,'sigmap-elk-'+plab+strcompress(string(aaa),/remove_all)+'.plot'
writeu, 12, in1
writeu, 12, jn1
writeu, 12, temp
close,12
;
 temp(*,*)=hatsh(*,*,kn/2,1)
close, 12
openw,12,'sigmah-ion-'+plab+strcompress(string(aaa),/remove_all)+'.plot'
writeu, 12, in1
writeu, 12, jn1
writeu, 12, temp
close,12
;
 temp(*,*)=hatsh(*,*,kn/2,2)
close, 12
openw,12,'sigmah-dst-'+plab+strcompress(string(aaa),/remove_all)+'.plot'
writeu, 12, in1
writeu, 12, jn1
writeu, 12, temp
close,12
;
 temp(*,*)=hatsh(*,*,kn/2,0)
close, 12
openw,12,'sigmah-elk-'+plab+strcompress(string(aaa),/remove_all)+'.plot'
writeu, 12, in1
writeu, 12, jn1
writeu, 12, temp
close,12
;

endif ; of save_plot

;
;
;======================== Elsasser number
;


; FIND
;


endfor ;   kkk cycle


end
