; solving Okuzumi's equation for Theta
; iterating over Mg as well
;
;
pro io_oku_iter_Mg, metals, numb_iter, dcnt, ioe, ngas, sound, ndust, sigma1, $
                      ava1, nimg, nimg2,nih2,    $
                    ioefr, ionfr, ndst1, dcharge, gfunk1, mion1 ;;; dchag1, dchargv ;;; ! rest is only good for testing

; recover grid size

ijkn=size(sound)
in=ijkn(1) & jn=ijkn(2) & kn=ijkn(3)

;parameters
; comment  nh2=ntot/(1.+2.*0.0851) rest is helium
;
;  ... eV is is cm^2 g /s^2
   mp=1.67262e-24
   me = 9.109d-28
   meanw= 2.3333
   k_B = 1.3806d-16 ; ;! in cm^2 g s-2 K-1
   N_A = 6.0d+23
   kcalpm = 4.19/N_A*1.d+3*1.d+4
   eV=1.602e-19*1.d+3*1.d+4
   ED = 5300.;      *kcalpm
   DeltaE= 2.d-3*eV/k_B ;!!
   mion=24.3050*mp
   nsites=1.5d+15
;   metals=  6.78e-07  ; 1.d-11 ; (Ilgner & Nelson '06)   6.78e-07 ;(in Dzyurkevich et al 2013?)
   mmet=24.3050*mp
   mhco=29.*mp
   mum=meanw*mp
   elk = 4.803d-10   ; statColoumb
   si = 1.d0
   se = 0.3d0
   tiny=1.d-99

; validate the input arays
; print, 'read init:', nimg(*,jn/2,kn/2), nih2(*,jn/2,kn/2)

helpfunk1=dblarr(in,jn,kn)

gfunk1=dblarr(in,jn,kn) 
mion1=dblarr(in,jn,kn)   & betat1=dblarr(in,jn,kn)
ioefr=dblarr(in,jn,kn) & ionfr=dblarr(in,jn,kn) 
gammy=dblarr(in,jn,kn) & niterh2=dblarr(in,jn,kn)  & niterh2(*,*,*)=0.d0
niterh3=dblarr(in,jn,kn) & niterh4=dblarr(in,jn,kn)  
th31=dblarr(in,jn,kn) &  helpfunk=0.d0
dcharge=dblarr(in,jn,kn)  & dchargv=dblarr(in,jn,kn) & dcharg1=dblarr(in,jn,kn)
ndst1=dblarr(in,jn,kn,dcnt+1)

;for i=0,in/2 do begin
;for j=jn/2-1, jn/2+1 do begin ;;;0,jn-1 do begin
;for k=kn/2-1,kn/2 do begin

           for i=0,in-1 do begin
           for k=0,kn-1 do begin
             for j=0,jn-1 do begin
yold= nimg(i,j,k)/nih2(i,j,k);;;;   1000.
y=yold
mark=0.d0

   temper=sound(i,j,k)^2*meanw/(N_A*k_B)
   beta11=3.d-11/sqrt(temper)
   beta22=3.d-6/sqrt(temper)
   alpha=3.d-9

temper=sound(i,j,k)^2*meanw/(N_A*k_B)
mion1(i,j,k)=(nih2(i,j,k)*mhco + nimg(i,j,k)*mmet)/(nih2(i,j,k)+nimg(i,j,k))
betat1(i,j,k)=(  beta11*nimg(i,j,k)+ beta22*nih2(i,j,k) )/(nih2(i,j,k)+nimg(i,j,k))
;print, 'VERY begin with',i,j,k, betat1(i,j,k)

goto, err2


err1: 

mark=1.d0

temper=sound(i,j,k)^2*meanw/(N_A*k_B)

mion1(i,j,k)=(nih2(i,j,k)*mhco + nimg(i,j,k)*mmet)/(nih2(i,j,k)+nimg(i,j,k))
betat1(i,j,k)=(  beta11*nimg(i,j,k)+ beta22*nih2(i,j,k) )/(nih2(i,j,k)+nimg(i,j,k))
;print, 'begin with',i,j,k, betat1(i,j,k)

yold=y

niterh2(i,j,k)=niterh2(i,j,k)+1.


err2: ; print, 'continue with Mg as a main ion


        ui=sqrt(8.*sound(i,j,k)^2*mum/!Pi/mion1(i,j,k))
        ue=ui*sqrt(mion1(i,j,k)/me ) ;! in cm /s

;--------------------------------------
; (non-dim parameter, Eq. 31)


; equation 31 (Okuzumi 09)

        th31(i,j,k) = ioe(i,j,k)*ngas(i,j,k)*elk^2 $  ; zeta*n_g*e^2
                     / (si * ui * sigma1(i,j) * ava1(i,j))   $    
                     / ( ( ndust(i,j,k) )^2 * k_B *temper )

;
;
; start iterations
;
     aa1=si*ui/(se*ue)

; eq. 29 in Okuzumi '09, or eq.12 in Dzyurkevich '13, same as gfunk below
    
         helpfunk= 2.* betat1(i,j,k) $
            * ioe(i,j,k)*ngas(i,j,k) $
            /( si*ui*se*ue *(sigma1(i,j)*ndust(i,j,k) )^2)

helpfunk1(i,j,k)=helpfunk

; Iteration of  Eq. 30 (OKuzumi '09),
;
;
xl=0.001
xh=4.d0
niter=40
ans=50.
for it=1,niter do begin
xm=0.5*(xl+xh)
yfunk1= helpfunk*exp(xl)/(1.+xl)
yfunk2= helpfunk*exp(xh)/(1.+xh)
yfunk3= helpfunk*exp(xm)/(1.+xm)
fl=1.d0/(1.d0+xl) -aa1*exp(xl) - xl/th31(i,j,k)*yfunk1/(sqrt(1.+2.*yfunk1)-1.)
fh=1.d0/(1.d0+xh) -aa1*exp(xh) - xh/th31(i,j,k)*yfunk2/(sqrt(1.+2.*yfunk2)-1.)
fm=1.d0/(1.d0+xm) -aa1*exp(xm) - xm/th31(i,j,k)*yfunk3/(sqrt(1.+2.*yfunk3)-1.)
;
sfunk=sqrt(fm^2-fl*fh)
if sfunk eq 0.d0 then begin
print, '714:fm, fl, fh, th31(i,j,k)=',fm, fl, fh, th31(i,j,k), '(', i,j,k,')'
print, '715:take ans! false braket!'

endif


if sfunk eq 0.d0 then gammy(i,j,k)=ans
xw=xm+(xm-xl)*(fl-fh)/abs(fl-fh) *fm/sfunk
ans=xw
; equation -->
yfunk= helpfunk*exp(xw)/(1.+xw)
fw=1.d0/(1.d0+xw) -aa1*exp(xw) - xw/th31(i,j,k)* yfunk/(sqrt(1.+2.*yfunk)-1.)
;
if fw eq 0.d0 then gammy(i,j,k)=xw
if xw lt xm then  begin
if (fw/fl lt 0.d0 and fw/fm gt 0.d0)  then  begin
xh=xw
fh=fw
endif
if (fw/fl gt 0.d0 and fw/fm lt 0.d0)  then  begin
xl=xw
fl=fw
xh=xm
fh=fm
endif
endif
if xw gt xm then  begin
if (fw/fh lt 0.d0 and fw/fm gt 0.d0)  then  begin
xl=xw
fl=fw
endif
if (fw/fh gt 0.d0 and fw/fm lt 0.d0)  then  begin
xl=xm
fl=fm
xh=xw
fh=fw
endif
endif

gammy(i,j,k)=xw
endfor
;-------------------------------------------------end of iterations-----

;print, 'intermediate-0', i,j,k, helpfunk, gammy(i,j,k)

;
;  dust mean charge and deviation

   dcharge(i,j,k)=-gammy(i,j,k)*ava1(i,j)/elk^2*k_B*temper
   dchargv(i,j,k)= (1.d0+gammy(i,j,k))/(2.d0+ gammy(i,j,k))*(ava1(i,j)*k_B*temper/elk^2 )

;
;  make gaussian over charges......

     for m1=0,dcnt do begin
      m=float(-dcnt/2+m1)+dcharge(i,j,k)
      ndst1(i,j,k,m1)= ndust(i,j,k)/sqrt(2.*!pi*dchargv(i,j,k) ) $
                       * exp( -0.5*( m -dcharge(i,j,k) )^2/dchargv(i,j,k) )
     endfor
;

     gfunk = 2.* betat1(i,j,k)    $
          *ioe(i,j,k)*ngas(i,j,k) $
          /( si*ui*se*ue *( sigma1(i,j)*ndust(i,j,k) )^2) $
         *exp(gammy(i,j,k))/(1.+gammy(i,j,k))

;print, 'intermediate-1',i,j,k, gfunk, betat1(i,j,k),ioe(i,j,k),gammy(i,j,k),ngas(i,j,k)

     gfunk1(i,j,k)= gfunk ; if need to save for debugging
;
;    Okuzumi's full ion concentration
;
; introduce a short-cut
if (gfunk lt 1.d-16) then begin
   ghelp=gfunk/2. 
endif else begin
   ghelp=(sqrt(1.+2.*gfunk)-1.+tiny)
endelse

; 
     ioefr(i,j,k) = ioe(i,j,k )  $
          * ngas(i,j,k)/ndust(i,j,k) $
          / (se*ue*sigma1(i,j)) *exp(gammy(i,j,k)) $
          * ghelp/gfunk
     ionfr(i,j,k) =ioe(i,j,k )  $
          * ngas(i,j,k)/ndust(i,j,k) $
          /(si*ui*sigma1(i,j)) /(1.+gammy(i,j,k))$
          * ghelp/gfunk

 
     dcharg1(i,j,k)= -(ionfr(i,j,k) - ioefr(i,j,k))/ndust(i,j,k)

;    for dust-free case only:


     ioend3=sqrt(  ioe(i,j,k) * ngas(i,j,k)   $
              / betat1(i,j,k)  )    

     if (temper ge 1500.) then begin
      print, 'TEMPERATURE >1500?', temper  
      ioefr(i,j,k) = ioend3
      ionfr(i,j,k) = ioend3
      dcharg1(i,j,k)=0.d0
      ndust(i,j,k)=0.0
     endif
;
;
;  ---  make check whether Mg is guessed correctly or not-----
;test
;   print, 'intermediate:',i,j,k, niterh2(i,j,k), ionfr(i,j,k), nimg(i,j,k), nih2(i,j,k), yold

   beta11=3.d-11/sqrt(temper)
   beta22=3.d-6/sqrt(temper)
   alpha=3.d-9
;
; here  solve quadratic equation to get n(HCO+)...
;
qea=alpha
qeb= (beta11*ionfr(i,j,k)+ui*si*sigma1(i,j)*ndust(i,j,k)+alpha*(nimg2(i,j,k)-ionfr(i,j,k)) )
qec=-beta11*ionfr(i,j,k)*ioefr(i,j,k)-ui*si*sigma1(i,j)*ndust(i,j,k)*ionfr(i,j,k)

test1=(-qeb + sqrt( qeb^2 - 4.*qea*qec )  )/(2.d0*qea)

nih2(i,j,k)=test1

if (test1 lt 0.0) then print, '830:Negative number density! for H2+'
;

check=dblarr(2)
check(0)=(ionfr(i,j,k)-test1) & check(1)= nimg2(i,j,k)
nimg(i,j,k)=min(check)


y=nimg(i,j,k)/nih2(i,j,k) 
ynew=y

betat1(i,j,k)=(beta11*y + beta22)/(1.d0+y)
mion1(i,j,k)=(nih2(i,j,k)*mhco + nimg(i,j,k)*mmet)/(nih2(i,j,k)+nimg(i,j,k))

;print, 'new densityes', i,j,k,niterh2(i,j,k), nih2(i,j,k)/ionfr(i,j,k), nimg(i,j,k)/ionfr(i,j,k),betat1(i,j,k),ynew,  (1.d0-ynew/yold)


; stop iterations if too small ynew, - one correction is enough, HCO+ dominates, betat1's already accurate
if (ynew lt 1.d-4) then goto, err3


; stop if number of iterations is too high, usually 10 is MORE then enough
if (niterh2(i,j,k) gt numb_iter) then goto, err3

; make iteration if difference is still high
if (abs(1.d0-ynew/yold) ge 1.d-2) then  goto, err1

err3:

niterh3(i,j,k)=abs(1.d0-ynew/yold)
niterh4(i,j,k)=ynew

endfor
endfor
endfor

;i=in/2
;print, 'io_Mg_iter::', ioefr(*,jn/2,kn/2), ionfr(*,jn/2,kn/2), $
   print, 'gfunk radial' ,          gfunk1(*,jn/2,kn/2)
;   print, 'gfunk vertical',     gfunk1(in/2,*,kn/2)
;   print, 'helpfunk vertical', helpfunk1(in/2,*,kn/2)
;   print, 'th31 vertical', th31(in/2,*,kn/2) 
;print, '---'
;   print, 'mion', mion1(in/2,*,kn/2)
;   print, 'ava1', ava1(in/2,*)
;   print, 'sigma1', sigma1(in/2,*)
;   print, 'ndust', ndust(in/2,*,kn/2)
;print, '---'

print, 'FINISHED iterating over Okuzumi Eq. 30 with adjusting Mg+; spread in dust charges=',dcnt, '  numb_iter=', numb_iter 

end







