
pro io_do_grid, in,jn,kn,rin,rout,Hscale, xlogscale, dx1a,dx2a,x1b,x2b

; use xlogscale=0 for normal x spacing,
; and xlogscale=1 for logarithmic spaced x-axis
; use Hscale 12 

           js = 2
           je = jn -3
           is = 2
           ie = in -3
           ks = 2
           ke = kn -3

;------------------------------------------------
; create the (r,Theta) grid
;

ciso = 0.03333

if (xlogscale eq 0) then begin

dx1a=(rout-rin) /float(in)
x1b = rin + findgen(in)*(rout-rin)/float(in-1)
  
endif else begin

x1b = 10.^(alog10(rin) + (alog10(rout)-alog10(rin))*findgen(in)/float(in-1))

endelse

dx2a=(2.*Hscale*ciso)/float(jn) 
dx3a=(!pi/4.) /float(kn)  ; not needed yet


x2a = !pi/2. -Hscale*ciso $  ; 1.270796326794896558
      +findgen(jn)*dx2a ;;; -2.*dx2a + findgen(je-js+5)*dx2a

x2b =  !pi/2. - Hscale*ciso  $  ;1.270796326794896558
      +findgen(jn)*dx2a + dx2a/2.;;; -2.*dx2a + findgen(je-js+5)*dx2a

; testing for symmetry
;for j=0,jn-1 do begin
;print, j, (x2a(j)-!pi/2.), (x2b(j) -!pi/2.)
;endfor

print, 'GRID finished'

end
