; making total ionization rate CR, XR and RN
; need: density, dust-to-gas ratio, grid...
;
pro io_rates,x1b,x2b, ngas,f_dg_t,$
             ioe, writefile=writefile



        mum=2.3333 * 1.67262e-24 ; meanw * mp
        ua=1.496d+13
        ioern = 7.d-19 

; Okuzumi's receipt for Xrays
        Lxr = 5.0d0 ;! in 10.d29 erg s-1
        ioecr0 = 1.d-17 ; 1.d-17 ;  5.d-18 ;! * 86400.d0  s-1 in yr-1
        ioexr0 = 2.6d-15 ;! * 86400.d0  s-1 in yr-1
        depcr = 96.d0 ;! g cm-2
        depxr =  8.d0 ;! g cm-2
; for 3 keV
        Lxr =  5.0d0 ;! in 1.*10.d29 erg s-1
        ioexr02 = 1.d-15 ;
        ioexr01 = 6.d-12 ; s^-1, after Bai & Goodmann 2009
        depxr1=1.5d+21 ; cm^-2 ; or 0.0064327356 g cm^-2
        depxr2=7.d+23  ; cm^-2 ; or  3.0019433   g cm^-2
        alp1= 0.4
        bet1=0.65
;   for 5 keV
;       Lxr = 1.0d0 ;! in 1.*10.d29 erg s-1  ??????????
       ioexr02b = 2.d-15 ;
       ioexr01b = 4.d-12 ; s^-1, after Bai & Goodmann 2009
       depxr1b=3.d+21 ; cm^-2
       depxr2b=1.d+24  ; cm^-2
       alp1b= 0.5
       bet1b= 0.7

; reconstruct the grid size
sz=size(ngas)
in=sz(1)
jn=sz(2)
kn=sz(3)
        js = 2
        je = jn -3
        is = 2
        ie = in -3
        ks = 2
        ke = kn -3
        dx2a=x2b(2)-x2b(1)
;-------------------

doben=dblarr(in,jn,kn)
duntn=dblarr(in,jn,kn)
ioe  =dblarr(in,jn,kn)
ioexro=dblarr(in,jn,kn)
ioecr =dblarr(in,jn,kn)
ioexro2=dblarr(in,jn,kn)


;=================2==========make surface and ionization==========
;
meanw=2.3333
mp= 1.67262e-24
mum=2.3333 * 1.67262e-24
abun1=meanw*mp*(1.d0+9.75*10.^(-2)+3.26*10.^(-4)+1.12*10.^(-4)+8.53*10.^(-4)+3.58*10.^(-5)+1.85*10.^(-5))

;
;
;c calcullate the vertically averaged slices of density

      doben(*,*,*)  = 0.d0
      duntn(*,*,*) = 0.d0
;       for k=ks-2,ke+2 do begin
        for i=is-2,ie+2 do begin
            doben(i,0,*) =ngas(i,0,*)*dx2a*x1b(i)*mum*ua
         for j= 1,je do begin
            doben(i,j,*) =ngas(i,j,*)*dx2a*x1b(i)*mum*ua + doben(i,j-1,*);! (density units)
           endfor
            duntn(i,je,*) = ngas(i,je,*)*dx2a*x1b(i)*mum*ua
           for j=1,je-1 do begin
                j2=je-j
            duntn(i,j2,*) = duntn(i,j2+1,*)+ngas(i,j2,*)*dx2a*x1b(i)*mum*ua
           endfor
         endfor ; i
;        endfor ; k
;
; 

        for i=is-2,ie+2 do begin
         for k=ks-2,ke+2 do begin
          for j=js-2,je+2 do begin

;c cosmic ray ionization - fit

   ioecr(i,j,k)=2.* ioecr0*5.d-1 $
                *( exp(- doben(i,j,k)/depcr)*(1.d0+(doben(i,j,k)/depcr)^(3./4.) $
                 )^(-4./3.) $
           +  exp(- duntn(i,j,k)/depcr)*(1.d0+(duntn(i,j,k)/depcr)^(3./4.) $
                 )^(-4./3.) )
; 3kV see Bai & Goodman 2009
   ioexro(i,j,k)=Lxr* (x1b(i))^(-2.2) * ( $
                         ioexr01 *( exp(- (doben(i,j,k)/depxr1/abun1)^alp1)+ exp(-(duntn(i,j,k)/depxr1/abun1)^alp1 ) )   +  $
                         ioexr02 *( exp(- (doben(i,j,k)/depxr2/abun1)^bet1)+ exp(-(duntn(i,j,k)/depxr2/abun1)^bet1) )      $
                                                )
; 5kV, same B&G 2009
;   ioexro2(i,j,k)=Lxr* (x1b(i))^(-2.2) * ( $
;                         ioexr01b *( exp(- (doben(i,j,k)/depxr1b/abun1)^alp1b)+ exp(-(duntn(i,j,k)/depxr1b/abun1)^alp1b) )   +  $
;                         ioexr02b *( exp(- (doben(i,j,k)/depxr2b/abun1)^bet1b)+ exp(-(duntn(i,j,k)/depxr2b/abun1)^bet1b))      $
;                                                )
;
;
   ioe(i,j,k)= ioexro(i,j,k)+ioecr(i,j,k)+ioern * (f_dg_t(i,j)/1.d-2)

endfor
endfor
endfor

print, 'IONIZATION RATES finished'

if (writefile gt 0) then  begin

plab='_io_'
aaa=strcompress(string(writefile),/remove_all)
inn=size(ioexr)
in1=long(41) & in1=inn(1)
jn1=long(41) & jn1=inn(2)

close, 11

temp=dblarr(in,jn)
temp(*,*)=ioexro(*,*,kn/2)
close, 11
openw,11,'XRbai3'+plab+strcompress(string(aaa),/remove_all)+'.plot'
writeu, 11, in1
writeu, 11, jn1
writeu, 11, temp
close,11

;temp(*,*)=ioexro2(*,*,kn/2)
;close, 11
;openw,11,'XRbai5'+plab+strcompress(string(aaa),/remove_all)+'.plot'
;writeu, 11, in1
;writeu, 11, jn1
;writeu, 11, temp
;close,11
;
temp(*,*)=ioecr(*,*,kn/2)
close, 11
openw,11,'CRokuz'+plab+strcompress(string(aaa),/remove_all)+'.plot'
writeu, 11, in1
writeu, 11, jn1
writeu, 11, temp
close,11


endif


end
